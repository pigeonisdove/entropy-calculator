# Entropy Calculator

## Description

A simple calculator for information entropy from an arbitrary value. Ignores whitespace. The goal is to run completely in the browser and use service workers to run offline and in an app-like mode on compatible devices such as smartphones. 

[See it on gitlab pages](pigeonisdove.gitlab.io/entropy-calculator/).

## Credits

Based on:

* `Weight Converter` described [here](https://bolajiayodeji.com/building-and-deploying-your-first-progressive-web-app/)
* `entropy.js` gist [here](https://gist.github.com/ppseprus/afab8500dec6394c401734cb6922d220#file-entropy-js)

