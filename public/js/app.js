const input = document.getElementById('textInput');
const output = document.getElementById('output');
const fineprint = document.getElementById('fine-print');

output.style.visibility = 'hidden';

const calculateEntropy = (e) => {
  let t = e.target.value;

  output.style.visibility = 'visible';
  if (fineprint.parentNode != output) {
    output.appendChild(fineprint);
  };

  let entropy = document.getElementById('entropyOutput');
  entropy.innerHTML = shannon.entropy(t);

  let normEntropy = document.getElementById('normEntropyOutput');
  normEntropy.innerHTML = shannon.normentropy(t);

  let metricEntropy = document.getElementById('metricEntropyOutput');
  metricEntropy.innerHTML = shannon.metricentropy(t);

  let totalEntropy = document.getElementById('totalEntropyOutput');
  totalEntropy.innerHTML = shannon.bits(t);

  e.preventDefault();
};

input.addEventListener('input', calculateEntropy);

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('../sw.js').then( () => {
      console.log('Service Worker Registered')
    })
  })
}
