// entropy.js MIT License © 2014 James Abney http://github.com/jabney
// ES6 portation MIT License © 2017 Peter Seprus http://github.com/ppseprus
// Adds more formulas MIT License © 2020 Pigeonisdove https://github.com/pigeonisdove

// based on https://gist.github.com/ppseprus/afab8500dec6394c401734cb6922d220#file-entropy-js

// Calculate the Shannon entropy of a string in bits per symbol.
(function (shannon) {
    'use strict';

    shannon.str = str => str.replace(/\s+/g,''); //removes whitespace
    const cstr = shannon.str //clean string

    shannon.len = str => cstr(str).length;
    const len = shannon.len;

    shannon.set = str => new Set(cstr(str));
    const set = shannon.set;
 
    // Build a frequency map from the string.
    shannon.frequencies = str => {
        return Array.from(cstr(str))
        .reduce((freq, c) => (freq[c] = (freq[c] || 0) + 1) && freq, {});
    };
    const freq = shannon.frequencies;

    // Sum the frequency of each character.
    shannon.entropy = str => {
        return Object.values(freq(cstr(str)))
        .reduce((sum, f) => {
            let p = f/len(cstr(str));
            return sum - p * Math.log2(p)
        }, 0);
    };

    // Measure the maximum entropy of a string in bits per symbol.
    shannon.maxentropy = str => {
     let unique = set(str).size;
      if (unique <= 1 ) { return 0 } else {
        return Math.log2(unique);
      }
    };

    // Measure the normalized entropy of a string in bits per symbol.
    shannon.normentropy = str => {
      if (len(str) <= 1 || shannon.maxentropy(str) == 0) { return 0 } else {
        return shannon.entropy(str) / shannon.maxentropy(str);
      }
    };

    // Measure the metric entropy of a string.
    shannon.metricentropy = str => {
      if (len(str) <= 1) { return 0 } else {
        return shannon.entropy(str) / len(str);
      }
    };

    // Measure the entropy of a string in total bits.
    shannon.bits = str => shannon.entropy(str) * len(str);

    // Log the entropy of a string to the console.
    shannon.log = str => console.log(
      `Length of "${str}":`,
      len(str),
      `\nEntropy of "${str}" in bits per symbol:`,
      shannon.entropy(str),
      `\nMaximum Entropy of "${str}":`,
      shannon.maxentropy(str),
      `\nNormalized Entropy of "${str}":`,
      shannon.normentropy(str),
      `\nMetric Entropy of "${str}":`,
      shannon.metricentropy(str),
      `\nTotal Entropy of "${str}" in bits:`,
      shannon.bits(str),
    );

})(window.shannon = window.shannon || Object.create(null));

shannon.log('1223334444');          // 1.8464393446710154
shannon.log('');                    // 0
shannon.log(' ');                   // 0
shannon.log('0');                   // 0
shannon.log('01');                  // 1
shannon.log('0123');                // 2
shannon.log('01234567');            // 3
shannon.log('0123456789abcdef');    // 4
shannon.log('15d35d84a737d0af');    // 3.202819531114783
